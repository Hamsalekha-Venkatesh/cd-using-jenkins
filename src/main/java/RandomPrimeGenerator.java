import java.util.LinkedList;
import java.util.List;

public class RandomPrimeGenerator {

    public static void main(String [] args) {
        RandomPrimeGenerator rpg = new RandomPrimeGenerator();
        System.out.print(rpg.primeNumbersTill(100));
    }

    public  List<Integer> primeNumbersTill(int n) {
        List<Integer> primeNumbers = new LinkedList<Integer>();
        if (n >= 2) {
            primeNumbers.add(2);
        }
        for (int i = 3; i <= n; i += 2) {
            if (isPrimeBruteForce(i)) {
                primeNumbers.add(i);
            }
        }
        return primeNumbers;
    }
    private boolean isPrimeBruteForce(int number) {
        for (int i = 2; i*i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
    
}
